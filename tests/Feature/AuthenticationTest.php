<?php

namespace ProvideSmart\NovaApi\Tests\Feature;

use Illuminate\Testing\TestResponse;
use ProvideSmart\NovaApi\Tests\Fixtures\Models\User;
use ProvideSmart\NovaApi\Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        User::create([
            'name'     => 'John Doe',
            'email'    => 'john@doe.com',
            'password' => bcrypt('12345678')
        ]);
    }

    public function test_it_returns_a_token_when_successfully_logging_in()
    {
        $response = $this->loginRequest('john@doe.com', '12345678');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'success',
            'data' => [
                'token',
                'user'
            ]
        ]);
    }

    public function test_it_returns_an_error_when_unsuccessfully_logging_in()
    {
        $response = $this->loginRequest('john1@doe.com', '12345678');

        $response->assertStatus(422);
        $response->assertJsonPath('success', false);
        $response->assertJsonStructure([
            'success',
            'message'
        ]);
    }

    public function test_it_has_access_to_protected_endpoints_with_a_token()
    {
        $response = $this->loginRequest('john@doe.com', '12345678');
        $json = $response->json();

        $userResponse = $this->get('/os-api/v1/me', [
            'Authorization' => 'Bearer ' . $json['data']['token'],
            'Accept'        => 'application/json'
        ]);

        $userResponse->assertStatus(200);
        $userResponse->assertJsonPath('data.email', 'john@doe.com');
        $userResponse->assertJsonStructure([
            'success',
            'data' => [
                'id',
                'email'
            ]
        ]);
    }

    public function test_it_returns_401_when_using_an_incorrect_authorization_token()
    {
        $userResponse = $this->get('/os-api/v1/me', [
            'Authorization' => 'Bearer 1234567890',
            'Accept'        => 'application/json'
        ]);

        $userResponse->assertStatus(401);
    }

    protected function loginRequest($email, $password): TestResponse
    {
        return $this->post('/os-api/v1/authenticate', [
            'email'    => $email,
            'password' => $password
        ], [
            'Accept' => 'application/json'
        ]);
    }
}
