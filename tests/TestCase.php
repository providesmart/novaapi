<?php

namespace ProvideSmart\NovaApi\Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use ProvideSmart\NovaApi\NovaApiServiceProvider;
use ProvideSmart\NovaApi\Tests\Fixtures\Models\User;

class TestCase extends \Orchestra\Testbench\TestCase
{
    use DatabaseMigrations;

    public function setUp(): void
    {
        parent::setUp();

        $this->loadLaravelMigrations();
    }

    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('auth.providers.users.model', User::class);
    }

    protected function getPackageProviders($app)
    {
        return [NovaApiServiceProvider::class];
    }
}
