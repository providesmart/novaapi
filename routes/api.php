<?php

use Illuminate\Support\Facades\Route;

Route::prefix('os-api')->middleware('api')->group(function () {

    Route::prefix('v1')->group(function () {

        Route::post('authenticate', [\ProvideSmart\NovaApi\Http\Controllers\AuthenticationController::class, 'authenticate']);

        Route::middleware('auth:sanctum')->group(function () {
            Route::get('me', [\ProvideSmart\NovaApi\Http\Controllers\AuthenticationController::class, 'me']);

            Route::get('search', [\ProvideSmart\NovaApi\Http\Controllers\SearchApiController::class, 'index']);
        });

    });

});
