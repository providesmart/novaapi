# NovaApi

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

Via Composer

``` bash
$ composer require outsmart/novaapi
```

## Usage

### Defining API endpoints

There are two different ways to define API endpoints for your Nova Resources. One way is to add a wildcard route that automatically adds routes for all of your resources. Another way to go is to add
an endpoint for a specific resource.

Endpoints for all Nova resources:

```php
Route::prefix('api')->middleware('api')->group(function () {
    Route::prefix('v1')->group(function () {
        NovaApi::routes();
    });
});
```

Endpoint for a specific resource:

```php
Route::prefix('os-api')->middleware('api')->group(function () {
    Route::prefix('v1')->group(function () {
        NovaApi::route(MyNovaResource::class)->only('index');
    });
});
```

### Querying data via GET parameters

On the resource's index endpoints there several query options available.

#### Searching

Searching is as passing your search value to the `seach` parameter, example;

`/products?search=Shoe`

#### Filtering

Column based filtering of the results is easy. You can use the simple syntax as wel as the advanced syntax to make use of other operators;

`/products?filters[brand]=Nike`

or

`/products?filters[name][like]=%Shoe%`

or

`/products?filters[price][gte]=50&filters[price][lte]=150`

or you can query a relation column (only one nest level)

`/products?warehouses.stock[gt]=0`

By default the where clauses are applied with an AND operator. If you want to use the OR operator just add `&filterJoin=OR` to your request uri.

**Available operators**

- gt (greater then)
- gte (greater then or equal)
- lt (less then)
- lte (less then or equal)
- eq (equal to)
- neq (no equal to)
- like

#### Pagination

Paginating requests is simple. Use the `page` and `perPage` parameters for limiting your results.

`/products?page=2&perPage=10`

#### Ordering

Ordering results can be performed in multiple ways:

```
// default order by price (asc)
/products?orderBy[price]=asc

// order by price and then by name (asc)
/products?orderBy[price]=asc&orderBy[name]=asc

// order descending
/products?orderBy[price]=desc

// order by price (asc) and by name (desc)
/products?orderBy[price]=asc&orderBy[name]=desc
```

#### Eager loading relationships

It is possible to eager load relationships with your request. Just specify the `with` parameter in your uri.

`/products?with=warehouses`

or

`/products?with[]=warehouses&with[]=suppliers`

**Auto eager loading relationships**

If you want to automatically eager load the relationships according to the defined fields you can specify the `autoEagerLoad` parameter:

`/products?autoEagerLoad=true`

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email author email instead of using the issue tracker.

## Credits

- [author name][link-author]
- [All Contributors][link-contributors]

## License

license. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/outsmart/novaapi.svg?style=flat-square

[ico-downloads]: https://img.shields.io/packagist/dt/outsmart/novaapi.svg?style=flat-square

[ico-travis]: https://img.shields.io/travis/outsmart/novaapi/master.svg?style=flat-square

[ico-styleci]: https://styleci.io/repos/12345678/shield

[link-packagist]: https://packagist.org/packages/outsmart/novaapi

[link-downloads]: https://packagist.org/packages/outsmart/novaapi

[link-travis]: https://travis-ci.org/outsmart/novaapi

[link-styleci]: https://styleci.io/repos/12345678

[link-author]: https://github.com/outsmart

[link-contributors]: ../../contributors
