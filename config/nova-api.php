<?php

return [

    /*
    |--------------------------------------------------------------------------
    | API Guard
    |--------------------------------------------------------------------------
    |
    | This option controls the default authentication "guard" and password
    | reset options for the API. You may change these defaults as
    | required, but they're a perfect start for most applications.
    |
    */
    'auth'          => [
        'guard' => 'api'
    ],

    /*
    |--------------------------------------------------------------------------
    | Batch Support
    |--------------------------------------------------------------------------
    |
    | When batch support is enabled, a batch of api calls can be executed
    | by posting to the batch endpoint. See documentation for further
    | explanation on the batching topic.
    |
    */
    'batch-support' => true,

];
