<?php


namespace ProvideSmart\NovaApi\Traits;


trait HasResource
{
    protected $resource;

    /**
     * Returns the coupled resource if it exists
     *
     * @return Resource|null
     */
    public function resource()
    {
        if (!is_null($this->resource)) {
            $class = $this->resource;
            return new $class($this);
        }

        if (class_exists($this->defaultResourceClass())) {
            $class = $this->defaultResourceClass();
            return new $class($this);
        }

        return null;
    }

    /**
     * Get the default resource class
     *
     * @return string|null
     */
    protected function defaultResourceClass()
    {
        try {
            $reflection = new \ReflectionClass($this);

            return $reflection->getNamespaceName() . '\\' . sprintf('Resources\\%sResource', $reflection->getShortName());
        } catch (\ReflectionException $e) {
            return null;
        }
    }
}
