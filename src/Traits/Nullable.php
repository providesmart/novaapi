<?php

namespace ProvideSmart\NovaApi\Traits;

trait Nullable
{
    public static function bootNullable()
    {
        static::saving(function ($model) {
            foreach ((array)$model->nullable as $key) {
                if (!$model->getAttribute($key)) {
                    $model->setAttribute($key, null);
                }
            }
        });
    }
}
