<?php

namespace ProvideSmart\NovaApi;

use Illuminate\Contracts\Routing\Registrar;
use ProvideSmart\NovaApi\Http\Controllers\ActionController;
use ProvideSmart\NovaApi\Http\Controllers\AssociatableApiController;
use ProvideSmart\NovaApi\Http\Controllers\AttachableApiController;
use ProvideSmart\NovaApi\Http\Controllers\BaseApiController;

class NovaApiRoutes
{
    /**
     * @var Registrar
     */
    private $router;

    public function __construct(Registrar $router)
    {
        $this->router = $router;
    }

    /**
     * Generate resource routes
     *
     */
    public function novaResourceRoutes()
    {
//        $this->router->prefix('os-api')->middleware(['api', 'auth:sanctum'])->group(function ($router) {

        $this->router->prefix('')->name('nova-api.')->group(function ($router) {
//            $router->get('{resource}/filters', [FilterApiController::class, 'index']);
//            $router->get('{resource}/filters/{filterName}', [FilterApiController::class, 'show']);

            $router->get('{resource}/actions', [ActionController::class, 'index']);
            $router->post('{resource}/actions', [ActionController::class, 'store']);

            $router->get('{resource}/associatable/{field}', [AssociatableApiController::class, 'index']);
            $router->get('{resource}/{resourceId}/attachable/{field}', [AttachableApiController::class, 'index']);

            $router->get('{resource}', [BaseApiController::class, 'index'])->name('index');
            $router->get('{resource}/{resourceId}', [BaseApiController::class, 'show'])->name('show');
            $router->post('{resource}', [BaseApiController::class, 'store'])->name('store');
            $router->put('{resource}/{resourceId}', [BaseApiController::class, 'update'])->name('update-put');
            $router->patch('{resource}/{resourceId}', [BaseApiController::class, 'update'])->name('update-patch');
            $router->delete('{resource}/{resourceId}', [BaseApiController::class, 'delete'])->name('delete');
        });

//        });
    }
}
