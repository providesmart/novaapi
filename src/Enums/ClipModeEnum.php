<?php

namespace ProvideSmart\NovaApi\Enums;

use Spatie\Enum\Enum;

/**
 * @method static self clip()
 * @method static self ellipsis()
 * @method static self none()
 */
class ClipModeEnum extends Enum
{

}
