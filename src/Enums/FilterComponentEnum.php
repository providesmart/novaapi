<?php

namespace ProvideSmart\NovaApi\Enums;

use Spatie\Enum\Enum;

/**
 * Class FilterComponentEnum
 *
 * @package ProvideSmart\NovaApi\Enums
 *
 * @method static self select()
 * @method static self text()
 * @method static self number()
 * @method static self boolean()
 * @method static self date()
 * @method static self range()
 */
class FilterComponentEnum extends Enum
{
    protected static function values(): array
    {
        return [
            'select'  => 'select-filter',
            'text'    => 'text-filter',
            'number'  => 'number-filter',
            'boolean' => 'boolean-filter',
            'date'    => 'date-range-filter',
            'range'   => 'range-filter',
        ];
    }
}
