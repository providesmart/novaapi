<?php

namespace ProvideSmart\NovaApi\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use ProvideSmart\NovaApi\Traits\HasResource;
use ProvideSmart\NovaApi\Traits\Nullable;

abstract class BaseModel extends Model
{
    use Nullable, HasResource, HasFactory;

    public function getSecondaryKey()
    {
        return $this->{$this->getSecondaryKeyName()} ?? null;
    }

    public function getSecondaryKeyName()
    {
        return $this->secondaryKey ?? null;
    }
}
