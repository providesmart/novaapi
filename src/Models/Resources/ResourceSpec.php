<?php

namespace ProvideSmart\NovaApi\Models\Resources;

use Illuminate\Database\Eloquent\Model;
use Laravel\Nova\Makeable;

class ResourceSpec
{
    use Makeable;

    public $attribute;
    public string $label;
    public array $icon;
    public $value;

    protected $resolveCallback;

    public function __construct(string $attribute, string $label, array $icon, $resolveCallback = null)
    {
        $this->attribute = $attribute;
        $this->label = $label;
        $this->icon = $icon;
        $this->resolveCallback = $resolveCallback;
    }

    public function resolve(Model $instance)
    {
        $this->value = $this->resolveCallback
            ? call_user_func($this->resolveCallback, $instance)
            : $instance->{$this->attribute};

        return $this;
    }

    public function toArray()
    {
        return [
            'attribute' => $this->attribute,
            'label'     => $this->label,
            'icon'      => $this->icon,
            'value'     => $this->value
        ];
    }
}
