<?php

namespace ProvideSmart\NovaApi\Models\Resources;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Laravel\Nova\Contracts\RelatableField;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\FieldCollection;
use Laravel\Nova\Http\Requests\NovaRequest;
use ProvideSmart\NovaApi\Nova\Actions\DeleteResourceAction;
use ProvideSmart\NovaApi\Classes\ColumnFilter;
use ProvideSmart\NovaApi\Models\Api\BaseApiResource;
use ProvideSmart\NovaApi\Models\Api\BaseApiResourceCollection;
use Titasgailius\SearchRelations\SearchesRelations;

abstract class Resource extends \Laravel\Nova\Resource
{
    use SearchesRelations;

    /**
     * Optionally defines a class that overrides the
     * default api behaviour for resources
     *
     * @var null
     */
    protected $apiOverrides = null;

    /**
     * Strip the class name from the 'Resource' suffix
     *
     * @return string
     */
    public static function uriKey()
    {
        return Str::plural(Str::kebab(str_replace('Resource', '', class_basename(get_called_class()))));
    }

    public function specs()
    {
        return [];
    }

    /**
     * Apply the search query to the query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $search
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery(\Illuminate\Database\Eloquent\Builder $query, string $search)
    {
        return static::applySearch($query, $search);
    }

    /**
     * Build an "index" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query;
    }

    /**
     * Build a Scout search query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Laravel\Scout\Builder $query
     * @return \Laravel\Scout\Builder
     */
    public static function scoutQuery(NovaRequest $request, $query)
    {
        return $query;
    }

    /**
     * Build a "detail" query for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function detailQuery(NovaRequest $request, $query)
    {
        return parent::detailQuery($request, $query);
    }

    /**
     * Build a "relatable" query for the given resource.
     *
     * This query determines which instances of the model may be attached to other resources.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function relatableQuery(NovaRequest $request, $query)
    {
        return parent::relatableQuery($request, $query);
    }

    public function filters(Request $request)
    {
        $autoFilters = [];

        foreach ($this->availableFields($request) as $field) {

            if ($field instanceof Field && $field->isFilterable()) {
                $autoFilters[] = new ColumnFilter($field, $this->resource);
            }

        }

        return $autoFilters;
    }

    /**
     * Transform resource object to an api resource
     *
     * @param NovaRequest $request
     * @param Model|null $model
     * @return array
     */
    public function resolveToArray(NovaRequest $request, Model $model = null)
    {
        $fields = $this->resolveFields($request);
        $hidden = $model->getHidden();

        return $fields
            ->reduce(function ($carry, $field) use ($model, $hidden) {

                if ($field instanceof RelatableField) {
                    if ($value = $this->relationValue($field, $model)) {
                        $carry[$field->attribute] = $value;
                    }

                    if ($field instanceof BelongsTo) {
                        $foreignKeyName = $model->{$field->attribute}()->getForeignKeyName();
                        $foreignKeyValue = $model->{$foreignKeyName};
                        $carry[$foreignKeyName] = $foreignKeyValue;
                    }
                } else {
                    if (!in_array($field->attribute, $hidden)) {
                        $carry[$field->attribute] = $field->value;
                    }
                }

                return $carry;
            }, []);
    }

    public function hasApiOverride($operation): bool
    {
        return $this->apiOverrides ? method_exists($this->apiOverrides, $operation) : false;
    }

    public function callApiOverride($operation, $arguments)
    {
        if ($this->hasApiOverride($operation)) {
            $override = new $this->apiOverrides;

            return call_user_func_array([$override, $operation], $arguments);
        }

        return false;
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            DeleteResourceAction::make()
        ];
    }

    /**
     * Get the validation rules for a resource creation request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public static function rulesForCreation(NovaRequest $request)
    {
        return static::formatRules($request, (self::newResource())
            ->creationFields($request)
            ->reject(function ($field) use ($request) {
                return $field->isReadonly($request) || !$field->forEnvironment($request->environment());
            })
            ->mapWithKeys(function ($field) use ($request) {
                return $field->getCreationRules($request);
            })->all());
    }

    /**
     * Get the validation rules for a resource update request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Laravel\Nova\Resource|null  $resource
     * @return array
     */
    public static function rulesForUpdate(NovaRequest $request, $resource = null)
    {
        $resource = $resource ?? self::newResource();

        return static::formatRules($request, $resource->updateFields($request)
            ->reject(function ($field) use ($request) {
                return $field->isReadonly($request) || !$field->forEnvironment($request->environment());
            })
            ->mapWithKeys(function ($field) use ($request) {
                return $field->getUpdateRules($request);
            })->all());
    }

    /**
     * Resolve the update fields.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return \Laravel\Nova\Fields\FieldCollection
     */
    public function updateFields(NovaRequest $request)
    {
        return $this->resolveFields($request, function ($fields) use ($request) {
            $fields = $this->removeNonUpdateFields($request, $fields);

            return $this->filterEnvironmentFields($request, $fields);
        });
    }

    public function filterEnvironmentFields(Request $request, FieldCollection $fields)
    {
        return $fields->reject(function(Field $field) use ($request) {
            return !$field->forEnvironment($request->environment());
        });
    }

//    /**
//     * Resolve the inverse field for the given relationship attribute.
//     *
//     * This is primarily used for Relatable rule to check if has-one / morph-one relationships are "full".
//     *
//     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
//     * @param  string  $attribute
//     * @param  string|null  $morphType
//     * @return \Laravel\Nova\Fields\FieldCollection
//     */
//    public function resolveInverseFieldsForAttribute(NovaRequest $request, $attribute, $morphType = null) {
//        $model = $request->model();
//
//        info('resolveInverseFieldsForAttribute ===== ' . get_class($model));
//
//        if (method_exists($request->resource()->model(), 'getMap')) {
//            $attribute = $model->getMap()[$attribute] ?? $attribute;
//        }
//
//        return parent::resolveInverseFieldsForAttribute($request, $attribute, $morphType);
//    }

    /**
     * @param Field $field
     * @param Model $model
     * @return BaseApiResource|BaseApiResourceCollection
     */
    protected function relationValue(Field $field, Model $model)
    {
        if ($model->relationLoaded($field->attribute)) {
            $relation = $model->getRelationValue($field->attribute);

            if ($relation instanceof Collection) {
                return BaseApiResourceCollection::make($relation);
            }

            return BaseApiResource::make($relation);
        }
    }
}
