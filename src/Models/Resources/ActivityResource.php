<?php

namespace ProvideSmart\NovaApi\Models\Resources;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Text;
use OutSmart\Core\Domain\Auth\Models\Resources\UserResource;
use ProvideSmart\NovaApi\Models\Activity;

class ActivityResource extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = Activity::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Log name'), 'log_name'),
            Text::make(__('Description'), 'description'),
            Text::make(__('Subject type'), 'subject_type'),
            Text::make(__('Subject ID'), 'subject_id'),
            Text::make(__('Causer type'), 'causer_type'),
            Text::make(__('Causer ID'), 'causer_id'),
            Text::make(__('Properties'), 'properties'),
            DateTime::make(__('Created at'), 'created_at'),
            DateTime::make(__('Updated at'), 'updated_at'),

            MorphTo::make(__('Causer'), 'causer')->types([
                UserResource::class
            ]),
//            MorphTo::make(__('Subject'), 'subject', [Model::class]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
