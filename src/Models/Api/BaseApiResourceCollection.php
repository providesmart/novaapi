<?php

namespace ProvideSmart\NovaApi\Models\Api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class BaseApiResourceCollection extends ResourceCollection
{
    public $additional = [
        'success' => true,
        'code'    => 200
    ];

    public function toArray($request)
    {
        return $this->collection;
    }
}
