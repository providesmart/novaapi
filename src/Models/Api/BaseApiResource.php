<?php

namespace ProvideSmart\NovaApi\Models\Api;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;
use ProvideSmart\NovaApi\Models\Resources\Resource;
use ProvideSmart\NovaApi\Traits\HasResource;

class BaseApiResource extends JsonResource
{
    public $additional = [
        'success' => true,
        'code'    => 200
    ];

    public function toArray($request)
    {
        $request = NovaRequest::createFrom($request);

        // Check if HasResource trait is used by resource
        $usesHasResource = in_array(HasResource::class, class_uses_recursive(get_class($this->resource)));

        if ($usesHasResource) {
            $attributes = $this->resolveResourceToArray($request);

            $pivot = $this->resource->pivot
                ? ['pivot' => $this->resource->pivot]
                : [];

            return array_merge($attributes, [
                '_title'    => $this->resource->resource()->title(),
                '_subtitle' => $this->resource->resource()->subtitle(),
                '_specs'    => collect($this->resource->resource()->specs())->map->resolve($this->resource)->toArray(),
            ], $pivot);
        }

        return $this->resource;
    }

    /**
     * If relational fields have null values make sure we
     * will return an empty array
     *
     * @param $request
     * @return mixed
     */
    public function resolveResourceToArray($request)
    {
        $attributes = $this->resource->resource()->resolveToArray($request, $this->resource);

        $this
            ->resource
            ->resource()
            ->availableFields($request)
            ->each(function (Field $field) use (&$attributes, $request) {
                if ($field->isRelation() && (!isset($attributes[$field->attribute]) || is_null($attributes[$field->attribute]))) {
                    $attributes[$field->attribute] = [];
                }
            });

        return $attributes;
    }
}
