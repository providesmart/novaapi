<?php

namespace ProvideSmart\NovaApi\Models;

use Spatie\Activitylog\Models\Activity as SpatieActivity;

/**
 * Class ActivityLog
 *
 * @package App\Models
 * @author Provide Software <software@provide.nl>
 * @mixin IdeHelperActivity
 */
class Activity extends SpatieActivity
{
//    public function causerUser(): BelongsTo
//    {
//        return $this->belongsTo(User::class, 'causer_id');
//    }
}
