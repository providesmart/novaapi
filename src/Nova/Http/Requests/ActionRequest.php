<?php

namespace ProvideSmart\NovaApi\Nova\Http\Requests;

use ProvideSmart\NovaApi\Classes\RequestCriteriaQuery;

class ActionRequest extends \Laravel\Nova\Http\Requests\ActionRequest
{

    /**
     * Transform the request into a query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function toQuery()
    {
        $resource = $this->resource();

        return $resource::buildIndexQuery(
            $this, $this->newQuery(), $this->search,
            $this->filters()->all(), $this->orderings(), $this->trashed()
        )->tap(function ($query) {
            // Tap into the original index query and add
            // our own RequestCriteriaQuery resolver
            RequestCriteriaQuery::resolve($this, $query);
        });
    }
}
