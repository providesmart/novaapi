<?php

namespace ProvideSmart\NovaApi\Nova\Actions;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Actions\DestructiveAction;
use Laravel\Nova\Fields\ActionFields;

class DeleteResourceAction extends DestructiveAction
{
    use InteractsWithQueue, Queueable, SerializesModels;

    public $meta = [
        'icon' => ['fal', 'trash-alt']
    ];

    public function name()
    {
        return __('Delete');
    }

    public function __construct()
    {
        $this->confirmText = __('Are you sure you want to delete your selection');
        $this->confirmButtonText = __('Delete');
        $this->cancelButtonText = __('Cancel');
    }

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        try {
            $models->each->delete();

            return Action::deleted();
        } catch (\Exception $e) {
            return Action::danger(__('Something went wrong while deleting the selection'));
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
