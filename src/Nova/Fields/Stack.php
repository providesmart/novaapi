<?php

namespace ProvideSmart\NovaApi\Nova\Fields;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\Line;

class Stack extends \Laravel\Nova\Fields\Stack
{
    /**
     * Prepare the stack for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(Field::jsonSerialize(), [
            'lines' => collect($this->lines)->map(function (Line $line) {
                return $line
                    ->withMeta([
                        'type' => $line->type
                    ])
                    ->jsonSerialize();
            }),
        ]);
    }
}
