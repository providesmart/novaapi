<?php

namespace ProvideSmart\NovaApi\Nova\Fields;

use Laravel\Nova\Fields\Field;

class Badge extends \Laravel\Nova\Fields\Badge
{
    /**
     * Prepare the element for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return array_merge(Field::jsonSerialize(), [
            'labels'  => $this->labels,
            'options' => $this->types,
        ]);
    }
}
