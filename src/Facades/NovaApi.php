<?php

namespace ProvideSmart\NovaApi\Facades;

use Illuminate\Routing\Router;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Route;
use Laravel\Ui\UiServiceProvider;
use ProvideSmart\NovaApi\Http\Controllers\BaseApiController;
use ProvideSmart\NovaApi\Http\Controllers\ResourceApiController;
use ProvideSmart\NovaApi\Models\Resources\Resource;
use ProvideSmart\NovaApi\NovaApiRoutes;
use ProvideSmart\NovaApi\NovaApiServiceProvider;
use RuntimeException;

class NovaApi extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'nova-api';
    }

    /**
     * Register the typical resource routes for an application.
     *
     * @param array $options
     */
    public static function routes($options = [])
    {
        static::providerIsLoaded();

//        Route::macro('novaResourceRoutes', function () {
//            return (new NovaApiRoutes())->novaResourceRoutes();
//        });
//
//        // Register routes after booting
//        // -> otherwise these routes will catch all requests
//        App::booted(function () {
//            static::$app->make('router')->novaResourceRoutes();
//        });

        Route::group($options, function ($router) {
            (new NovaApiRoutes($router))->novaResourceRoutes();
        });
    }

    public static function route(string $resourceClass, $options = [])
    {
        static::providerIsLoaded();

        /** @var Router $router */
        $router = static::$app->make('router');

        /** @var Resource $resource */
        $resource = new $resourceClass($resourceClass::$model);

        return $router->apiResource($resource->uriKey(), BaseApiController::class, $options);
    }

    public static function resourceRoute()
    {
        Route::get('resources/{resource}', [ResourceApiController::class, 'show']);
    }

    protected static function providerIsLoaded()
    {
        if (!static::$app->providerIsLoaded(NovaApiServiceProvider::class)) {
            throw new RuntimeException('In order to use the NovaApi::routes() method, please install the outsmart/novaapi package.');
        }
    }
}
