<?php

namespace ProvideSmart\NovaApi\Classes;

class ApiResponse
{
    // TODO: implement extendable formats
//    public const successFormat = [
//        'success' => true,
//        'data'    => '#{data}',
//        'code'    => '#{code}'
//    ];

    public static function default($data, $code = 200)
    {
        return response()->json($data, $code);
    }

    public static function success($data, $code = 200)
    {
        return static::default([
            'success' => true,
            'data'    => $data,
            'code'    => $code
        ], $code);
    }

    public static function error($errors, $message = null, $code = 422)
    {
        return static::default([
            'success' => false,
            'message' => $message,
            'errors'  => $errors,
            'code'    => $code
        ], $code);
    }

    public static function unauthorized($code = 401)
    {
        return static::default([
            'success' => false,
            'code'    => $code
        ], $code);
    }

    public static function forbidden($code = 403)
    {
        return static::default([
            'success' => false,
            'code'    => $code
        ], $code);
    }
}
