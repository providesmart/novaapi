<?php

namespace ProvideSmart\NovaApi\Classes;

use Illuminate\Support\Str;
use Laravel\Nova\Filters\Filter;

abstract class AbstractFilter extends Filter
{
    public $searchable = false;
    public $sticky = false;
    public $api = false;

    protected ?string $resource;
    protected ?string $relatedResourceName = null;

    public function id()
    {
        return $this->id ?? Str::slug($this->name);
    }

    /**
     * Prepare the filter for JSON serialization.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $data = parent::jsonSerialize();

        return array_merge($data, [
            'id'                  => $this->id(),
            'attribute'           => $this->attribute ?? null,
            'searchable'          => $this->searchable,
            'sticky'              => $this->sticky,
            'relatedResourceName' => $this->relatedResourceName ?? null,
            'api'                 => $this->api
        ]);
    }
}
