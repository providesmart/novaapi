<?php

namespace ProvideSmart\NovaApi\Classes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\FieldCollection;
use Laravel\Nova\Http\Requests\NovaRequest;

class RequestCriteriaQuery
{
    protected array $operators = [
        'eq'   => '=',
        'gt'   => '>',
        'lt'   => '<',
        'gte'  => '>=',
        'lte'  => '<=',
        'neq'  => '!=',
        'like' => 'like',
        'in'   => 'in'
    ];

    protected string $filterJoin = 'AND';

    protected NovaRequest $request;

    protected Builder $query;

    protected string $resource;

    protected Model $model;

    public function __construct(NovaRequest $request, &$query = null, string $resource = null, Model $model = null)
    {
        $this->request = $request;
        $this->query = $query ?? $request->newQuery();
        $this->resource = $resource ?? $request->resource();
        $this->model = $model ?? $request->model();
    }

    public static function resolve(NovaRequest $request, &$query = null, string $resource = null, Model $model = null): Builder
    {
        return (new static($request, $query, $resource, $model))->handle();
    }

    public function handle(): Builder
    {
        $this->parseViaRelationship();
        $this->parseWith();
        $this->parseFilterJoin();
        $this->parseFilters();
        $this->parsePredefinedFilters();
        $this->parseOrderBy();
        $this->parseSearch();

        return $this->query;
    }

    private function parseViaRelationship()
    {
        $viaRelationship = $this->request->get('viaRelationship');
        $viaRelationshipId = $this->request->get('viaRelationshipId');

        if ($this->model->isRelation($viaRelationship) && $viaRelationship && $viaRelationshipId) {
            $this->query->whereHas($viaRelationship, function (Builder $query) use ($viaRelationshipId) {
                $query->whereKey($viaRelationshipId);
            });
        }
    }

    private function parseWith(): void
    {
        if ($this->request->get('autoEagerLoad')) {
            $this->query->with($this->determineEagerLoadedRelationships());
        }

        if ($with = $this->request->get('with')) {
            $this->query->with($with);
        }

        if ($with = $this->resource::$with ?? null) {
            $this->query->with($with);
        }
    }

    private function parseFilterJoin(): void
    {
        $this->filterJoin = in_array(strtoupper($this->request->get('filterJoin')), ['AND', 'OR'])
            ? strtoupper($this->request->get('filterJoin'))
            : $this->filterJoin;
    }

    private function parsePredefinedFilters(): void
    {
        if ($this->request->get('predefinedFilters')) {
            $filters = $this->request->get('predefinedFilters', []);

            foreach ($filters as $filterId => $filterValue) {
                if ($filterClass = $this->getPredefinedFilter($filterId)) {
                    $this->query = $filterClass->apply($this->request, $this->query, $filterValue);
                }
            }
        }
    }

    private function parseFilters(): void
    {
        $filterableFields = $this->getFilterableFields();
        $filters = collect($this->request->get('filters', []));

        $filterableFields->each(function ($field) use ($filters) {

            if ($field->isRelation() && $relationFilters = $this->relationFiltersFromRequest($field->attribute)) {

                $this->query->whereHas($field->attribute, function ($relationQuery) use ($relationFilters, $field) {
                    foreach ($relationFilters[$field->attribute] as $relationField => $value) {
                        $this->applyConditions($this->getMappedField($relationField, $field->attribute), $value, $relationQuery);
                    }
                });

                return;
            }

            $filterValue = $filters->get($field->attribute);
            $fieldName = $this->getMappedField($field->attribute);

            $this->applyConditions($fieldName, $filterValue);
        });
    }

    private function applyConditions($field, $value, $query = null): void
    {
        switch (gettype($value)) {
            case 'string':
            case 'integer':
                $this->addCondition($field, '=', $value, $query);
                break;

            case 'array':
                $this->addGroupedCondition(function ($query) use ($field, $value) {
                    foreach ($value as $operator => $value) {
                        $operator = $this->resolveOperator($operator);
                        if ($operator === 'in') {
                            $query->whereIn($field, $value);
                        } else {
                            $query->where($field, $operator, $value);
                        }
                    }
                }, $query);
                break;
        }
    }

    private function parseOrderBy(): void
    {
        $orderBy = $this->request->get('orderBy', []);

        $this->orderBy($orderBy);
    }

    private function parseSearch(): void
    {
        $search = $this->request->get('search', null);

        if ($search) {
            $this->query = $this->resource::searchQuery($this->query, $search);
        }
    }

    private function getFilterableFields(): FieldCollection
    {
        return $this->request
            ->newResource()
            ->availableFields($this->request)
            ->filter(function ($field) {
                return $field->filterable ?? true;
            });
    }

    private function getRelationFields(): FieldCollection
    {
        return $this->request
            ->newResource()
            ->availableFields($this->request)
            ->filter(function ($field) {
                return $field->isRelation();
            });
    }

    private function getMappedField($attribute, $relation = null)
    {
        $model = $relation
            ? $this->model->{$relation}()->getRelated()
            : $this->model;

        if (method_exists($model, 'getMap')) {
            $modelMap = $model->getMap();

            return $modelMap->get($attribute);
        }

        return $attribute;
    }

    private function addCondition(string $field, string $operator, $filterValue, $query = null): void
    {
        $query = $query ?? $this->query;

        $query->{$this->determineWhereClause()}($field, $operator, $filterValue);
    }

    private function addGroupedCondition(\Closure $closure, $query = null): void
    {
        $query = $query ?? $this->query;

        $query->{$this->determineWhereClause()}(function ($query) use ($closure) {
            $closure($query);
        });
    }

    private function determineWhereClause(): string
    {
        return $this->filterJoin == 'AND'
            ? 'where'
            : 'orWhere';
    }

    private function resolveOperator(string $operator): string
    {
        if (!isset($this->operators[$operator])) {
            throw new \Exception('Operator `' . $operator . '` does not exist');
        }

        return $this->operators[$operator];
    }

    private function orderBy($orderBy): void
    {
        $index = 0;
        foreach ($orderBy as $attribute => $direction) {
            $this->query->orderBy($this->getMappedField($attribute), $direction ?? 'asc');

            $index++;
        }
    }

    private function relationFiltersFromRequest($attribute)
    {
        $params = collect($this->request->get('filters', []));

        return $params
            ->map(function ($param, $key) use ($attribute) {
                return [
                    'key'   => $key,
                    'value' => $param
                ];
            })
            ->reduce(function ($carry, $param) use ($attribute) {

                $relationKey = $attribute . '.';

                if (Str::startsWith($param['key'], $relationKey)) {
                    $relationAttribute = Str::replaceFirst($relationKey, '', $param['key']);

                    $carry[$attribute][$relationAttribute] = $param['value'];
                }

                return $carry;
            }, []);
    }

    private function getPredefinedFilter($filterId): AbstractFilter
    {
        return $this
            ->availablePredefinedFilters()
            ->first(function ($predefinedFilter) use ($filterId) {
                return $predefinedFilter->id() == $filterId;
            });
    }

    private function availablePredefinedFilters(): Collection
    {
        return $this->request->newResource()->availableFilters($this->request);
    }

    private function determineEagerLoadedRelationships(): array
    {
        return $this->getRelationFields()
            ->map(function (Field $field) {
                return $field->attribute;
            })
            ->all();
    }
}
