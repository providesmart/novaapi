<?php

namespace ProvideSmart\NovaApi\Classes;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Select;
use ProvideSmart\NovaApi\Enums\FilterComponentEnum;
use ProvideSmart\NovaApi\Nova\Fields\Badge;

class ColumnFilter extends AbstractFilter
{
    /**
     * @var Field
     */
    private $field;
    protected ?string $resource;
    protected ?string $relatedResourceName = null;

    protected $attribute;

    public function __construct(Field $field, ?string $resource)
    {
        $this->name = $field->filterName ?? $field->name;
        $this->attribute = $field->attribute;
        $this->field = $field;
        $this->resource = $resource;

        $this->initializeFilter();
    }

    public function options(Request $request)
    {
        switch (get_class($this->field)) {
            case \Laravel\Nova\Fields\Badge::class:
            case Badge::class:
                return array_map(function($option, $key) {
                    return [
                        'label' => $option,
                        'value' => $key
                    ];
                }, array_values($this->field->labels), array_keys($this->field->labels));
            case \OutSmart\Core\Domain\Support\Nova\Fields\Select::class:
            case Select::class:
                return $this->field->meta['options'];
            case BelongsTo::class:
            case HasMany::class:
                $resourceClass = $this->field->resourceClass;
                $relatedModel = $resourceClass::$model;

                if ($relatedModel::count() <= 20) {
                    return $relatedModel::all()
                        ->map(function($model) use ($resourceClass) {
                            return [
                                'label' => (new $resourceClass($model))->title(),
                                'value' => $model->getKey()
                            ];
                        });
                }

                $this->api = true;

                return [];
        }

        return [];
    }

    public function initializeFilter()
    {
        $this->sticky = $this->field->isStickyFilter();

        switch (get_class($this->field)) {

            case Currency::class:
            case Number::class:
                $this->component = FilterComponentEnum::range();
                break;

            case DateTime::class:
                $this->component = FilterComponentEnum::date();
                break;

            case Boolean::class:
                $this->component = FilterComponentEnum::boolean();
                break;

            case Select::class:
            case \OutSmart\Core\Domain\Support\Nova\Fields\Select::class:
            case Badge::class:
                $this->component = FilterComponentEnum::select();
                break;

            case BelongsTo::class:
            case HasMany::class:
                $this->relatedResourceName = $this->field->resourceName ?? null;
                $this->searchable = true;
                $this->component = FilterComponentEnum::select();
                break;

            default:
                $this->component = FilterComponentEnum::text();
                break;
        }

        return [];
    }

    public function apply(Request $request, $query, $value)
    {

    }
}
