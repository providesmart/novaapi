<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Laravel\Nova\Http\Controllers\AttachableController;
use Laravel\Nova\Http\Requests\NovaRequest;
use ProvideSmart\NovaApi\Classes\RequestCriteriaQuery;
use ProvideSmart\NovaApi\Models\Api\BaseApiResourceCollection;

class AttachableApiController extends AttachableController
{
    /**
     * List the available related resources for a given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     * @return BaseApiResourceCollection
     */
    public function index(NovaRequest $request)
    {
        $field = $request->newResource()
            ->availableFields($request)
            ->filterForManyToManyRelations()
            ->firstWhere('resourceName', $request->field);

        $withTrashed = $this->shouldIncludeTrashed(
            $request, $associatedResource = $field->resourceClass
        );

        $parentResource = $request->findResourceOrFail();

        $viaResource = [
            'key'     => $parentResource->resource->getKey(),
            'name'    => $parentResource->singularLabel(),
            'display' => $parentResource->title(),
        ];

        $query = $field->buildAttachableQuery($request, $withTrashed)->toBase();

        $resource = $field->resourceClass;

        $collection = RequestCriteriaQuery::resolve($request, $query, $resource)
            ->paginate($request->get('perPage'))
            ->appends($request->except('page'));

        return BaseApiResourceCollection::make(
            $collection
        )->additional([
            'relations'   => array_keys($query->getEagerLoads()),
            'viaResource' => $viaResource
        ]);
    }
}
