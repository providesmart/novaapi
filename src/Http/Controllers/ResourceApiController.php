<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Routing\Controller;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\ResourceIndexRequest;

class ResourceApiController extends Controller
{
    public function show(Request $request, $resourceKey)
    {
        $request->route()->setParameter('resource', $resourceKey);

        $request = ResourceIndexRequest::createFrom($request);

        $resource = $request->newResource();
        $fields = $resource->availableFields($request);

        $id = 1;
        $fields = $fields
            ->filter(function ($field) use ($request) {
                return $field->forEnvironment($request->environment());
            })
            ->map(function ($field) use ($resourceKey, &$id, $resource) {

                if ($field instanceof Field) {

                    $field->withMeta([
                        'id'                => $resourceKey . '.' . $field->attribute . '.' . $id,
                        'foreignKey'        => $field instanceof BelongsTo
                            ? $this->getRelationForeignKeyName($resource->model(), $field->attribute)
                            : $field->attribute,
                        'resource'          => $resourceKey,
                        'isRelation'        => $field->isRelation(),
                        'isComputed'        => $field->computed(),
                        'isQuickCreate'     => $field->isQuickCreate(),
                        'showInHeading'     => $field->shouldShowInHeading(),
                        'editableInHeading' => $field->shouldBeEditableInHeading(),
                        'visibility'        => [
                            'showOnIndex'    => $field->showOnIndex,
                            'showOnDetail'   => $field->showOnDetail,
                            'showOnCreation' => $field->showOnCreation,
                            'showOnUpdate'   => $field->showOnUpdate,
                            'hideByDefault'  => $field->isHiddenByDefault()
                        ]
                    ]);
                }

                $id++;

                return $field;
            })
            ->all();

        $response = [
            'id'      => $resourceKey,
            'fields'  => $fields,
            'actions' => $resource->availableActions($request),
            'filters' => $resource->availableFilters($request),
            'lenses'  => $resource->availableLenses($request),
        ];

        return JsonResource::make($response);
    }

    /**
     * Get foreign key name for relation.
     *
     * @param \Illuminate\Database\Eloquent\Relations\Relation $relation
     * @return string
     */
    protected function getRelationForeignKeyName(Model $model, string $attribute)
    {
        /** @var Relation $relation */
        $relation = $model->{$attribute}();
        $foreignKey = method_exists($relation, 'getForeignKeyName')
            ? $relation->getForeignKeyName()
            : $relation->getForeignKey();

        if (method_exists($model, 'getMappingByValue')) {
            return $model->getMappingByValue($foreignKey);
        }

        return $foreignKey;
    }
}
