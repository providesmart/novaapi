<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Laravel\Nova\Http\Controllers\FilterController;
use Laravel\Nova\Http\Requests\NovaRequest;

class FilterApiController extends FilterController
{
    /**
     * List the filters for the given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     */
    public function index(NovaRequest $request)
    {
        return response()->json($request->newResource()->availableFilters($request));
    }

    public function show(NovaRequest $request, $resource, $filterName)
    {
        $filters = $request->newResource()->availableFilters($request);

        $filter = $filters->first(function ($filter) use ($filterName) {
            return $filter->name === $filterName;
        });

        return $filter;
    }
}
