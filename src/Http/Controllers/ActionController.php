<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Laravel\Nova\Http\Requests\ActionRequest;

class ActionController extends \Laravel\Nova\Http\Controllers\ActionController
{
    /**
     * Perform an action on the specified resources.
     *
     * @param \Laravel\Nova\Http\Requests\ActionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActionRequest $request)
    {
        $request = \ProvideSmart\NovaApi\Nova\Http\Requests\ActionRequest::createFrom($request);

        info('REQUEST CLASS', [get_class($request)]);

        $request->validateFields();

        return $request->action()->handleRequest($request);
    }
}
