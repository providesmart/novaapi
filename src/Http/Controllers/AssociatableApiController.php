<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Laravel\Nova\Contracts\RelatableField;
use Laravel\Nova\Http\Requests\NovaRequest;
use ProvideSmart\NovaApi\Classes\RequestCriteriaQuery;
use ProvideSmart\NovaApi\Models\Api\BaseApiResourceCollection;

class AssociatableApiController extends \Laravel\Nova\Http\Controllers\AssociatableController
{
    /**
     * List the available related resources for a given resource.
     *
     * @param \Laravel\Nova\Http\Requests\NovaRequest $request
     */
    public function index(NovaRequest $request)
    {
        $field = $request->newResource()
            ->availableFields($request)
            ->whereInstanceOf(RelatableField::class)
            ->findFieldByAttribute($request->field);

        $withTrashed = $this->shouldIncludeTrashed(
            $request, $associatedResource = $field->resourceClass
        );

        $request->route()->setParameter('resource', $field->resourceClass::uriKey());


        $query = $field->buildAssociatableQuery($request, $withTrashed)->toBase();

        $collection = RequestCriteriaQuery::resolve($request, $query)
            ->paginate($request->get('perPage'))
            ->appends($request->except('page'));

        return BaseApiResourceCollection::make(
            $collection
        )->additional([
            'relations' => array_keys($query->getEagerLoads())
        ]);

//        return [
//            'resources' => $field->buildAssociatableQuery($request, $withTrashed)->get()
//                ->mapInto($field->resourceClass)
//                ->filter->authorizedToAdd($request, $request->model())
//                ->map(function ($resource) use ($request, $field) {
//                    return $field->formatAssociatableResource($request, $resource);
//                })->sortBy('display')->values(),
//            'softDeletes' => $associatedResource::softDeletes(),
//            'withTrashed' => $withTrashed,
//        ];
    }
}
