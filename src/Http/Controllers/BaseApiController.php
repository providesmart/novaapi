<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Routing\Controller;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;
use Laravel\Nova\Resource;
use ProvideSmart\NovaApi\Classes\ApiResponse;
use ProvideSmart\NovaApi\Classes\RequestCriteriaQuery;
use ProvideSmart\NovaApi\Models\Api\BaseApiResource;
use ProvideSmart\NovaApi\Models\Api\BaseApiResourceCollection;
use ProvideSmart\NovaApi\Models\BaseModel;

class BaseApiController extends Controller
{
    public function __construct(NovaRequest $request)
    {
        $this->resolveResource($request);
    }

    public function index(NovaRequest $request)
    {
        $resource = $request->newResource();
        $resource->authorizeToViewAny($request);

        $query = RequestCriteriaQuery::resolve($request);

        $collection = $query
            ->paginate($request->get('perPage'))
            ->appends($request->except('page'));

        return BaseApiResourceCollection::make(
            $collection
        )->additional([
            'relations' => array_keys($query->getEagerLoads())
        ]);
    }

    public function show(NovaRequest $request, $resource, $modelId)
    {
        /** @var BaseModel $model */
        $model = $request->model()->newQuery()->findOrFail($modelId);

        $model->load(array_merge($this->determineEagerLoadedRelationships($request), $request->get('with', [])));

        $model->resource()->authorizeToView($request);

        return BaseApiResource::make($model);
    }

    public function store(NovaRequest $request)
    {
        /** @var Resource $resource */
        $resource = $request->resource();
        $resource::authorizeToCreate($request);

        $validation = $resource::validatorForCreation($request);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'errors'  => $validation->errors()
            ], 422);
        }

        if (!$model = $request->newResource()->callApiOverride('store', func_get_args())) {
            $result = $resource::fill($request, $request->model());

            // FIXME: IS THIS NEEDED?
            $test = (new $resource($request->model()))->creationFieldsWithoutReadonly($request);

            $model = $result[0];

            $model->save();
        }

        $model = $request->model()->newQuery()->findOrFail($model->id);

        $model->load($this->determineEagerLoadedRelationships($request));

        return ApiResponse::success(BaseApiResource::make($model));
    }

    public function update(NovaRequest $request, $resource, $modelId)
    {
        /** @var Resource $resource */
        $resource = $request->resource();
        /** @var BaseModel $model */
        $model = $request->findModelOrFail($modelId);

        $request->newResource()->authorizeToUpdate($request);

        if ($request->isMethod('PATCH')) {
            $request->merge(
                array_merge($model->toArray(), $request->all())
            );
        }

        $this->hydrateRelationshipObjects($request);

        $validation = $request->resource()::validatorForUpdate($request);

        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'errors'  => $validation->errors()
            ], 422);
        }

        if (!$updatedModel = $request->newResource()->callApiOverride('update', func_get_args())) {
            $result = $resource::fillForUpdate($request, $model);

            $updatedModel = $result[0];
            $updatedModel->save();
        }

        $updatedModel = $request->model()->newQuery()->findOrFail($updatedModel->id);

        $updatedModel->load($this->determineEagerLoadedRelationships($request));

        return BaseApiResource::make($updatedModel)
            ->additional([
                'success' => true,
                'code'    => 200
            ]);
    }

    public function delete(NovaRequest $request, $resource, $modelId)
    {
        /** @var BaseModel $model */
        $model = $request->findModelOrFail($modelId);

        $model->resource()->authorizeToDelete($request);

        if (!$model = $request->newResource()->callApiOverride('delete', func_get_args())) {
            $model->delete();
        }

        return BaseApiResource::make($model)
            ->additional([
                'success' => true,
                'code'    => 200
            ]);
    }

    protected function applyFilters(NovaRequest $request, Builder &$query)
    {
        $this->applyUrlParamFilters($request, $query);

//        $this->applyPaginationFilters();
    }

    protected function applyUrlParamFilters(NovaRequest $request, Builder &$query)
    {
        $filterableFields = $request
            ->newResource()
            ->availableFields($request)
            ->filter(function ($field) {
                return $field->filterable ?? true;
            });

        $filterableFields->each(function ($field) use ($request, $query) {
            if ($request->has($field->attribute)) {

                $value = $request->get($field->attribute);
                $map = $request->model()->getMap();
                $mappedField = $map->get($field->attribute);

                if (is_string($value)) {
                    $query->where($mappedField, $value);
                } elseif (is_array($value)) {
                    $query->where(function ($query) use ($request, $mappedField, $value) {
                        foreach ($value as $operator => $v) {
                            $query->where($mappedField, $this->resolveOperator($operator), $v);
                        }
                    });
                }
            }
        });
    }

    protected function resolveOperator($urlOperator)
    {
        $operatorMap = [
            'eq'   => '=',
            'gt'   => '>',
            'lt'   => '<',
            'gte'  => '>=',
            'lte'  => '<=',
            'neq'  => '!=',
            'like' => 'like'
        ];

        return $operatorMap[$urlOperator] ?? '=';
    }

    protected function resolveResource(NovaRequest $request)
    {
        if (!$request->route('resource') && $resource = Nova::resourceInstanceForKey($request->segment(3))) {
            $request->route()->setParameter('resource', $request->segment(3));
        }
    }

    private function determineEagerLoadedRelationships(NovaRequest $request): array
    {
        return $this->relationFields($request)
            ->map(function (Field $field) {
                return $field->attribute;
            })
            ->all();
    }

    private function relationFields(NovaRequest $request)
    {
        return $request
            ->newResource()
            ->availableFields($request)
            ->filter(function ($field) {
                return $field->isRelation();
            });
    }

    private function hydrateRelationshipObjects(NovaRequest &$request)
    {
        $relationFields = $this->relationFields($request);
        $relationKeys = $relationFields->map(function ($field) {
            return $field->attribute;
        });

        foreach ($relationKeys as $relationKey) {
            $field = $relationFields->where('attribute', $relationKey)->first();
            $relationValue = $request->get($relationKey);

            if (is_string($relationValue)) {
                try {
                    $relationValue = json_decode($relationValue, true);

                    if (!$field->isMultiRelation()) {
                        $relationValue = $relationValue['id'];
                    } else {
                        $relationValue = array_map(function ($model) {
                            return $model->id;
                        }, $relationValue ?? []);
                    }
                } catch (\Exception $e) {

                }
            }

            if (is_array($relationValue)) {
                $request->replace(array_merge(
                    $request->toArray(),
                    [
                        $relationKey => $relationValue
                    ]
                ));
            }
        }
    }
}
