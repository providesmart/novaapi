<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\NewAccessToken;
use ProvideSmart\NovaApi\Classes\ApiResponse;
use ProvideSmart\NovaApi\Http\Requests\AuthenticateRequest;
use ProvideSmart\NovaApi\Tests\Fixtures\Models\User;

class AuthenticationController extends Controller
{
    public function authenticate(AuthenticateRequest $request)
    {
        $guard = config('nova-api.auth.guard', config('auth.defaults.guard')) ?? config('auth.defaults.guard');

        config(['auth.defaults.guard' => $guard]);

        if (Auth::attempt($request->only(['email', 'password']))) {

            /** @var User $user */
            $user = Auth::user();

            $token = $user->createToken('spa_login');

            return $this->authenticateResponse($user, $token);
        }

        return ApiResponse::error([], __('Incorrect login credentials'));
    }

    public function me(Request $request)
    {
        return $this->meResponse($request->user());
    }

    protected function authenticateResponse($user, NewAccessToken $token)
    {
        return response()->json([
            'success' => true,
            'data'    => [
                'token' => $token->plainTextToken,
                'user'  => $user
            ]
        ]);
    }


    protected function meResponse($user)
    {
        return response()->json([
            'success' => true,
            'data'    => $user
        ]);
    }
}
