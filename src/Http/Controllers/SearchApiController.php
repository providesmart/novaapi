<?php

namespace ProvideSmart\NovaApi\Http\Controllers;

use Illuminate\Routing\Controller;
use Laravel\Nova\GlobalSearch;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Nova;

class SearchApiController extends Controller
{
    public function index(NovaRequest $request)
    {
        $resources = $this->getApplicableResources($request);

        return (new GlobalSearch(
            $request, $resources
        ))->get();
    }

    protected function getApplicableResources($request)
    {
        $whitelist = $request->get('resources') ?? [];

        return collect(Nova::globallySearchableResources($request))
            ->filter(function ($resource) use ($whitelist) {
                return !count($whitelist) || in_array($resource::uriKey(), $whitelist);
            })
            ->all();
    }
}
