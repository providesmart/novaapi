<?php

namespace ProvideSmart\NovaApi\Http\Middleware;

use Closure;

class NovaApiMiddleware
{
    public function handle($request, Closure $next)
    {
//        $guard = config('nova-api.auth.guard', config('auth.defaults.guard')) ?? config('auth.defaults.guard');
//
//        config(['auth.defaults.guard' => $guard]);

        return $next($request);
    }
}
