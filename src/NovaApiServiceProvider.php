<?php

namespace ProvideSmart\NovaApi;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Laravel\Nova\Fields\Field;
use Laravel\Sanctum\SanctumServiceProvider;
use ProvideSmart\NovaApi\Http\Middleware\NovaApiMiddleware;
use Spatie\Activitylog\ActivitylogServiceProvider;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NovaApiServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'outsmart');
        $this->mergeConfigFrom(__DIR__ . '/../config/nova-api.php', 'nova-api');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/api.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        $this->registerMacros();

        $this
            ->app
            ->make(Router::class)
            ->pushMiddlewareToGroup('api', NovaApiMiddleware::class);
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/nova-api.php', 'nova-api');

        // Register the service the package provides.
        $this->app->singleton('nova-api', function ($app) {
            return new NovaApi;
        });

        $this->app->register(SanctumServiceProvider::class);
        $this->app->register(ActivitylogServiceProvider::class);

        $this->registerExceptionHandlers();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['nova-api'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/nova-api.php' => config_path('nova-api.php'),
        ], 'nova-api.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/outsmart'),
        ], 'novaapi.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/outsmart'),
        ], 'novaapi.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/outsmart'),
        ], 'novaapi.views');*/

        // Registering package commands.
        // $this->commands([]);
    }

    private function registerMacros()
    {
        Route::macro('novaResourceRoute', function ($model = null, $uri = null, $options = []) {
            (new NovaApiRoutes())->novaResourceRoute($model, $uri);
        });

        Field::macro('filterable', function ($enabled = true) {
            $this->filterable = $enabled;

            return $this;
        });

        Field::macro('isFilterable', function () {
            return $this->filterable ?? false;
        });

        Field::macro('filterName', function ($name) {
            $this->filterName = $name;

            return $this;
        });

        Field::macro('stickyFilter', function ($enabled = true) {
            $this->stickyFilter = $enabled;

            return $this;
        });

        Field::macro('isStickyFilter', function () {
            return $this->stickyFilter ?? false;
        });

        Field::macro('hideByDefault', function ($enabled = true) {
            $this->hideByDefault = $enabled;

            return $this;
        });

        Field::macro('isHiddenByDefault', function () {
            return $this->hideByDefault ?? false;
        });

        Field::macro('showInHeading', function ($enabled = true, $editable = false) {
            $this->showInHeading = $enabled;
            $this->editableInHeading = $editable;

            return $this;
        });

        Field::macro('shouldShowInHeading', function () {
            return $this->showInHeading ?? false;
        });

        Field::macro('shouldBeEditableInHeading', function () {
            return $this->editableInHeading ?? false;
        });

        Field::macro('isRelation', function () {
            return in_array($this->component, [
                'has-one-field',
                'has-many-field',
                'belongs-to-field',
                'belongs-to-many-field',
                'morph-one-field',
                'morph-many-field',
                'morph-to-field',
                'morph-to-many-field',
                'nova-attach-many'
            ]);
        });

        Field::macro('isMultiRelation', function () {
            return in_array($this->component, [
                'has-many-field',
                'belongs-to-many-field',
                'morph-many-field',
                'morph-to-many-field',
                'nova-attach-many'
            ]);
        });

        Field::macro('quickCreate', function ($quickCreate = true) {
            $this->quickCreate = $quickCreate;
            return $this;
        });

        Field::macro('isQuickCreate', function () {
            return $this->quickCreate ?? false;
        });

        Field::macro('environment', function($environments = []) {
            $this->environments = $environments;

            return $this;
        });

        Field::macro('forEnvironment', function($environment) {
            return isset($this->environments) && count($this->environments)
                ? in_array($environment, $this->environments)
                : true;
        });

        Request::macro('environment', function($environment = null) {
            /** @var $this Request */
            if ($this->header('environment')) {
                return $environment ? $this->header('environment') === $environment : $this->header('environment');
            }
        });
    }

    private function registerExceptionHandlers()
    {
        if ($this->app->runningInConsole() && !$this->app->runningUnitTests()) {
            return;
        }

        /** @var ExceptionHandler $exceptionHandler */
        $exceptionHandler = resolve(ExceptionHandler::class);

        $exceptionHandler->renderable(function (NotFoundHttpException $e, Request $request) use ($exceptionHandler) {
            if ($request->wantsJson()) {
                return response()->json([
                    'success' => false,
                    'message' => __('Endpoint not found')
                ], 404);
            }
        });
    }
}
